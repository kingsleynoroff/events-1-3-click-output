# Basic click event 3

When the button on the page is clicked, output "The button was clicked" onto the page (not in the console).  The button should remain on the page after it is clicked.

_Note:_ The styles are provided for you in master.css, to apply up the styles, use the *clickbutton* class on the button and the *output* class on the output element.

This is the [link to example](https://events-1-3-click-output.now.sh)

## Task

Clone or download this repository onto your computer.  You will start out in the "master" branch which contains an empty project.

Try to recreate the website above.  Firstly, try to create it without any help.  If you are unsure of what to do, you can follow the steps below.  If the steps don't help, checkout out the "answer" branch from this repository.  The answer branch contains a working example.

## Steps

1. Add a button into the HTML, make sure to add the clickbutton class so the styles are applied
1. After the button add a DIV with the class name of "output".
2. Select the button and div in JavaScript.
3. Add an event listener onto the button.
4. Create a callback function that is passed into the event listener.  The function should *add* "The button was clicked" onto the innerHTML of the div.